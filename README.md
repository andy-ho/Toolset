#Toolset

> 常用工具集整理。

## instsrv

要把应用程序添加为服务，你需要两个小软件：Instsrv.exe和Srvany.exe。Instsrv.exe可以给系统安装和删除服务，Srvany.exe可以让程序以服务的方式运行。这两个软件都包含在Windows NT Resource Kit里，如果你没有，也可以在本站下载 instsrv.exe和 srvany.exe。
srvany.exe可用于将任何EXE程序作为Windows服务运行。也就是说srvany只是其注册程序的服务外壳，这个特性对于我们来说非常实用，我们可以通过它让我们的程序以SYSTEM账户启动，或者实现随机器启动而自启动，也可以隐藏不必要的窗口，比如说控制台窗口等等。

如何使用：
方法1，手动：
略。

方法2，批处理半自动。
1.将所要注册的服务程序（例：Console.exe）也放到该目录下。
2.安装服务：install.bat  MyService Console.exe
3.卸载服务：remove.bat MyService